<!--
IF YOU DON'T WANT TO FORK AND CREATE MERGE REQUEST, PLEASE DON'T CREATE ISSUE. FIRST CREATE ISSUE, THEN WAIT FOR AT LEAST 5 WORKING DAYS FOR MY CONFIRMATION.
-->

## URL of the post

<!--
URL of the post
-->

## typos list

<!--
provide list of typos
-->

**incorrect word** 
**correct word** 

**incorrect word** 
**correct word** 

**incorrect word** 
**correct word** 

## if you have created merge request

<!--
did you mention `resolves#xxx` [ **xxx** is the issue number ] in your merge request or merge request comment
-->

- [ ] yes
- [ ] no

/label ~typo
